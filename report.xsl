<?xml version="1.0" encoding="ISO-8859-1"?>

<xsl:stylesheet version="1.0" xmlns:xsl="http://www.w3.org/1999/XSL/Transform">
  <xsl:template match="/">
    <html>
      <head>
        <script type="text/javascript" src="http://code.jquery.com/jquery-1.7.2.min.js"></script>
        <!-- <script type="text/javascript" src="jquery-1.7.2.min.js"></script> -->
        <script type="text/javascript">                                         
          // we will add our javascript code here                                     

          function update_status()
          {
            $.get('status', function(content) {
              $('#status').html(content); });
          }

          $(document).ready(function() {
          update_status();
          setInterval(update_status,5000);
          });

        </script>                                                               

        <title>Test log for <xsl:value-of select="testreport/name"/></title>
        <link href="report.css" rel="stylesheet" type="text/css"/>
      </head>
      <body>
        <h1>Test log for <xsl:value-of select="testreport/name"/></h1>

        <p>Status: <span id="status"></span></p>

        <table class="log">
          <xsl:for-each select="testreport/commit">
            <tr>
              <th colspan="4" class="log"><a href = "{url}" title = "Source"><xsl:value-of select="date"/></a></th>
            </tr>
            <tr>
              <xsl:for-each select="testrun">
                <td colspan="4">
                  <div class="tests">
                    <table class="tests">
                      <xsl:if test="count(build) > 0">
                        <tr><td colspan="6" class="buildtime">
                          Build time: 
                          <xsl:value-of select="build/duration"/> minutes
                        </td></tr>

                      </xsl:if>
                      <xsl:if test="count(allpass) = 1">
                        <tr>
                          <th colspan="3" class="buildfail">
                            <a href="{url}" title="{date}">All tests passed</a>
                          </th>
                        </tr>
                      </xsl:if>

                      <xsl:if test="count(invalid) = 1">
                        <tr>
                          <th colspan="3" class="buildfail">
                            Invalid test data
                          </th>
                        </tr>
                      </xsl:if>

                      <xsl:for-each select="buildfail">
                        <tr>
                          <th colspan="3" class="buildfail">
                            <a href="{logurl}">Build failure</a>
                          </th>
                        </tr>
                      </xsl:for-each>
                      
                      <xsl:if test="count(failedtest) > 0">
                        <tr><th colspan="3" class="alltests">Tests failed</th></tr>
                        <tr>
                          <th class="tests">Thorn</th>
                          <th class="tests">Test</th>
                          <th class="tests"># procs</th>
                          <th class="tests">Log</th>
                          <th class="tests">Diffs</th>
                          <th class="tests">Data</th>
                        </tr>
                        <xsl:for-each select="failedtest">
                          <tr>
                            <td class="tests"><xsl:value-of select="thorn"/></td>
                            <td class="tests"><xsl:value-of select="name"/></td>
                            <td class="tests"><xsl:value-of select="nprocs"/></td>
                            <td class="tests"><a href="{logurl}" title="Test log">log</a></td>
                            <td class="tests">
                              <xsl:if test="count(diffsurl) > 0">
                                <a href="{diffsurl}" title="Test diffs">diffs</a>
                              </xsl:if>
                            </td>
                            <td class="tests">
                              <xsl:if test="count(dataurl) > 0">
                                <a href="{dataurl}" title="Test data">data</a>
                              </xsl:if>
                            </td>
                          </tr>
                        </xsl:for-each>
                      </xsl:if>
                    </table>
                  </div>
                </td>
              </xsl:for-each>
            </tr>

            <xsl:for-each select="submodule">
              <xsl:for-each select="commit">
                <tr>
                  <td class="commits"><xsl:value-of select="../shortname"/></td>
                  <td class="commits"><a href="{url}" title="Commit details"><xsl:value-of select="subject"/></a></td>
                </tr>
              </xsl:for-each>
            </xsl:for-each>

            
          </xsl:for-each>
        </table>
      </body>
    </html>
  </xsl:template>
</xsl:stylesheet>
